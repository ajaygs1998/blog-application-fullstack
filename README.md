# blog-application-fullstack
    1. client wants a application where she/he can write blogs and articles.
    2. user can comment on articles.

# techinical Requirements:
    1. user should create delete update and list blogs.
    2. user should add upate delete and list posts.
    3. categories the post according to categories.
    4. new user should able to resister on  application.
    5. User should able to login to our application.
    6. Blog can contain the image file.
    7. sort the blogs and pagination.
    8. proper login and Register api.
    9. Proper exception handling.
    10. Role Based Authentication-based securiy with apis.
    11. JWT based authenticaion.
    12. document all rest apis so that consumer can use 
    13. Deploy backend application on any cloud platform.

# tools used:
    1. java 8+
    2. Maven
    3. STS
    4. Apache Tomcat
    5. Spring Core, spring Security, Spring data JPA(Hibernate).
    6. MySQL
    7. PostMan
    8. Swagger --api documentation.
    9. AWS EC2 --deploying.

# Architecture: 

![alt text](https://www.tutorialandexample.com/wp-content/uploads/2020/02/Spring-Boot-Architecture-2.png)

# use of the layers:
 
 ![alt text](https://www.tutorialandexample.com/wp-content/uploads/2020/02/Spring-Boot-Architecture.png)




