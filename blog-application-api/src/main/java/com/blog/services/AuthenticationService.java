package com.blog.services;

import com.blog.payloads.requests.LoginRequest;
import com.blog.payloads.requests.PasswordUpdateRequest;
import com.blog.payloads.requests.SignUpRequest;
import com.blog.payloads.response.JwtResponse;
import com.blog.payloads.response.MessageResponse;

public interface AuthenticationService {
	
	//login
	JwtResponse authenticateUser(LoginRequest loginRequest);
	
	//register
	MessageResponse registerUser(SignUpRequest signupRequest);
	
    MessageResponse forgetPassword(String email, PasswordUpdateRequest passwordUpdateRequest);
}
