package com.blog.services;

import java.util.List;

import com.blog.payloads.UserDTO;

public interface UserService {

	UserDTO createUser(UserDTO user);

	UserDTO upateUser(UserDTO user, Long userId);

	UserDTO getUserByUserId(Long userId);

	List<UserDTO> getAllUsers();

	void deleteUser(Long userId);

}
