package com.blog.services;

import java.util.List;

import com.blog.payloads.CategoryDTO;

public interface CategoryService {

	
	CategoryDTO createCategory(CategoryDTO CategoryDTO);

	CategoryDTO upateCategory(CategoryDTO CategoryDTO, Long CategoryId);

	CategoryDTO getCategoryById(Long CategoryId);

	List<CategoryDTO> getAllCategories();

	void deleteCategory(Long CategoryId);
}
