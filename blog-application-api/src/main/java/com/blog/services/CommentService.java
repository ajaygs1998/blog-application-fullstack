package com.blog.services;

import com.blog.payloads.CommentDTO;

public interface CommentService {
	
	CommentDTO createComment(CommentDTO commentDTO,Long postId);
	
	 void deleteComment(Long commentId);
}
