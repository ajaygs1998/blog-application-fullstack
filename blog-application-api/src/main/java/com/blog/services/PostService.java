package com.blog.services;

import java.util.List;

import com.blog.entities.Post;
import com.blog.payloads.PostDTO;
import com.blog.payloads.PostResponse;

public interface PostService {

	PostDTO createPost(PostDTO PostDTO, Long userId, Long categoryId);

	PostDTO upatePost(PostDTO PostDTO, Long PostId);

	PostDTO getPostByPostId(Long PostId);

	PostResponse getAllPosts(Integer pageNumber, Integer pageSize, String sortBy, String sortDirection);

	void deletePost(Long PostId);

	// get all post by category
	List<PostDTO> getPostByCategory(Long categoryId);

	// get all post by user
	List<PostDTO> getPostByUser(Long userId);

	//search post
	List<PostDTO> getPostByKeyword(String keyword);
}
