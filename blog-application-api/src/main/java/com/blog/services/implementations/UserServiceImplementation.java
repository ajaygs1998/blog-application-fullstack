package com.blog.services.implementations;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blog.entities.User;
import com.blog.exceptions.ResourceNotFoundException;
import com.blog.payloads.UserDTO;
import com.blog.repositories.UserRepository;
import com.blog.services.UserService;

@Service
public class UserServiceImplementation implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	ModelMapper modelMapper;

	@Override
	public UserDTO createUser(UserDTO userDTO) {
		User user = this.dtoToUser(userDTO);
		User savedUser = this.userRepository.save(user);
		return this.userToDTO(savedUser);
	}

	@Override
	public UserDTO upateUser(UserDTO userDTO, Long userId) {
		User user = this.userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException(" User ", " Id ", userId));
		user.setUsername(userDTO.getUsername());
		user.setEmail(userDTO.getEmail());
		user.setPassword(userDTO.getPassword());
		// user.setAbout(userDTO.getAbout());
		User updatedUser = this.userRepository.save(user);
		UserDTO userDTO1 = this.userToDTO(updatedUser);
		return userDTO1;
	}

	@Override
	public UserDTO getUserByUserId(Long userId) {

		// Spring 3.0 deprecated getOne() and getById()
		User user = this.userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException(" User ", " Id ", userId));
		return this.userToDTO(user);
	}

	@Override
	public List<UserDTO> getAllUsers() {
		List<User> users = this.userRepository.findAll();
		List<UserDTO> userDTOs = users.stream().map(user -> this.userToDTO(user)).collect(Collectors.toList());
		return userDTOs;
	}

	@Override
	public void deleteUser(Long userId) {
		// first this will find the user, if user exists then it will delete otherwise,
		// it will throw exception.
		User user = this.userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException(" User ", " Id ", userId));
		this.userRepository.delete(user);
	}

	public User dtoToUser(UserDTO userDTO) {
		User user = modelMapper.map(userDTO, User.class);
		return user;
	}

	public UserDTO userToDTO(User user) {
		UserDTO userDTO = modelMapper.map(user, UserDTO.class);
		return userDTO;
	}

}
