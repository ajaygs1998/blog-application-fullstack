package com.blog.services.implementations;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.blog.entities.Category;
import com.blog.entities.Post;
import com.blog.entities.User;
import com.blog.exceptions.ResourceNotFoundException;
import com.blog.payloads.CategoryDTO;
import com.blog.payloads.PostDTO;
import com.blog.payloads.PostResponse;
import com.blog.payloads.UserDTO;
import com.blog.repositories.CategoryRepository;
import com.blog.repositories.PostRepository;
import com.blog.repositories.UserRepository;
import com.blog.services.PostService;

@Service
public class PostServiceImplementation implements PostService {

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public PostDTO createPost(PostDTO PostDTO, Long userId, Long categoryId) {

		User user = this.userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException(" User ", " userId", userId));
		
		Category category = this.categoryRepository.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundException(" Category ", " categoryId", categoryId));

		Post post = this.modelMapper.map(PostDTO, Post.class);
		 
		post.setImageName("defaultImage.png");
		post.setAddedDate(new Date());
		post.setUser(user);
		post.setCategory(category);
		Post savedPost = this.postRepository.save(post);
		return this.modelMapper.map(savedPost, PostDTO.class);
	}

	@Override
	public PostDTO upatePost(PostDTO postDTO, Long postId) {

		Post post = this.postRepository.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "post id", postId));
		post.setTitle(postDTO.getTitle());
		post.setContent(postDTO.getContent());
		post.setImageName(postDTO.getImageName());
		Post updatedPost = this.postRepository.save(post);

		return this.modelMapper.map(updatedPost, PostDTO.class);
	}

	@Override
	public PostDTO getPostByPostId(Long postId) {
		Post post = this.postRepository.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "postId", postId));
		return this.modelMapper.map(post, PostDTO.class);
	}

	@Override
	public PostResponse getAllPosts(Integer pageNumber, Integer pageSize, String sortBy, String sortDirection) {
		// Pagination and sort
		Sort sort = (sortDirection.equalsIgnoreCase("asc")) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
		Pageable page = PageRequest.of(pageNumber, pageSize, sort);
		Page<Post> pagePost = this.postRepository.findAll(page);
		List<Post> allposts = pagePost.getContent();

		List<PostDTO> posts = allposts.stream().map(p -> this.modelMapper.map(p, PostDTO.class))
				.collect(Collectors.toList());
		PostResponse postResponse = new PostResponse();

		postResponse.setContent(posts);
		postResponse.setPageNumber(pagePost.getNumber());
		postResponse.setPageSize(pagePost.getSize());
		postResponse.setTotalElement(pagePost.getTotalElements());
		postResponse.setTotalPages(pagePost.getTotalPages());
		postResponse.setLastPage(pagePost.isLast());
		return postResponse;

	}

	@Override
	public void deletePost(Long postId) {
		Post post = this.postRepository.findById(postId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "post id: ", postId));
		this.postRepository.delete(post);

	}

	@Override
	public List<PostDTO> getPostByCategory(Long categoryId) {
		Category category = this.categoryRepository.findById(categoryId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "category Id", categoryId));

		List<Post> posts = this.postRepository.findByCategory(category);
		List<PostDTO> postDTOs = posts.stream().map(p -> this.modelMapper.map(p, PostDTO.class))
				.collect(Collectors.toList());
		return postDTOs;
	}

	@Override
	public List<PostDTO> getPostByUser(Long userId) {
		User user = this.userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("Post", "userId", userId));
		List<Post> userPosts = this.postRepository.findByUser(user);
		List<PostDTO> postDTOs = userPosts.stream().map(u -> this.modelMapper.map(u, PostDTO.class))
				.collect(Collectors.toList());
		return postDTOs;

	}

	@Override
	public List<PostDTO> getPostByKeyword(String keyword) {
		List<Post> postBykeyWord = this.postRepository.findByKeyword(keyword);
		List<PostDTO> postDTOs = postBykeyWord.stream().map(p -> this.modelMapper.map(p, PostDTO.class))
				.collect(Collectors.toList());

		return postDTOs;
	}

}
