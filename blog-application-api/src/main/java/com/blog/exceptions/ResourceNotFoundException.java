package com.blog.exceptions;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter

@NoArgsConstructor
public class ResourceNotFoundException extends RuntimeException{
	
	String resourceName;
	String fieldName;
	long fieldValue;
	String userName;
	//for almost all methods
	public ResourceNotFoundException(String resourceName, String fieldName, long fieldValue) {
		super(String.format("%s not found with %s: %s", resourceName, fieldName, fieldValue));
		this.resourceName = resourceName;
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
	}
//for finding the user
	public ResourceNotFoundException(String resourceName, String fieldName, String userName) {
		super(String.format("%s not found with %s: %s", resourceName, fieldName, userName));
		this.resourceName = resourceName;
		this.fieldName = fieldName;
		this.userName = userName;
	}
	


}
