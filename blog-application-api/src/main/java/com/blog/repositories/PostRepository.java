package com.blog.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.blog.entities.Category;
import com.blog.entities.Post;
import com.blog.entities.User;

public interface PostRepository extends JpaRepository<Post, Long> {

	List<Post> findByUser(User user);

	List<Post> findByCategory(Category user);

	@Query("SELECT p FROM Post p WHERE CONCAT(p.content,' ', p.title) LIKE %?1%")
	List<Post> findByKeyword(String post);
}
/*
 * while the searching the post in the front end highlight matching keyword
 * dynamically.
 */

