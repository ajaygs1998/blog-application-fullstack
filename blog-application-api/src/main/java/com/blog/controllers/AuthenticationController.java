package com.blog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blog.payloads.requests.LoginRequest;
import com.blog.payloads.requests.PasswordUpdateRequest;
import com.blog.payloads.requests.SignUpRequest;
import com.blog.payloads.response.JwtResponse;
import com.blog.payloads.response.MessageResponse;
import com.blog.repositories.RoleRepository;
import com.blog.repositories.UserRepository;
import com.blog.security.jwt.JwtUtils;
import com.blog.services.AuthenticationService;

import jakarta.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;
     
	
	
	@Autowired
	private AuthenticationService authenticationService;

	@PostMapping("/signin")
	public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		JwtResponse jwtResponse = authenticationService.authenticateUser(loginRequest);
		return new ResponseEntity<>(jwtResponse, HttpStatus.CREATED);
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		MessageResponse messageResponse = authenticationService.registerUser(signUpRequest);
		return new ResponseEntity<>(messageResponse, HttpStatus.CREATED);
	}
	
	@PutMapping("/forgetpassword/{email}")
	public ResponseEntity<MessageResponse> forgetPassword(@Valid @PathVariable String email ,@RequestBody PasswordUpdateRequest passwordUpdateRequest) {
		//PasswordUpdateRequest updatedPassword = this.authenticationService.forgetPasword(email);
		MessageResponse messageResponse=this.authenticationService.forgetPassword(email, passwordUpdateRequest);
		return new ResponseEntity<>(messageResponse, HttpStatus.OK);
	}
	
	

}
