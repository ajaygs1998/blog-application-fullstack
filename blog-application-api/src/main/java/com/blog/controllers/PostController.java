package com.blog.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.blog.config.AppConstants;
import com.blog.payloads.ApiResponse;
import com.blog.payloads.PostDTO;
import com.blog.payloads.PostResponse;
import com.blog.services.FileService;
import com.blog.services.PostService;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/")

public class PostController {
	private static Logger logger = LoggerFactory.getLogger(PostController.class);
	@Autowired
	private PostService postService;

	@Autowired
	private FileService fileService;

	@Value("project.image")
	private String path;

	// Post createPost(PostDTO PostDTO, Long userId, Long categoryId);
	@PostMapping("/user/{userId}/category/{categoryId}/posts")
	//@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<PostDTO> createPost(@Valid @RequestBody PostDTO postDTO, @PathVariable Long userId,
			@PathVariable Long categoryId) {
		PostDTO createPost = this.postService.createPost(postDTO, userId, categoryId);

		return new ResponseEntity<PostDTO>(createPost, HttpStatus.CREATED);
	}

	@GetMapping("/category/{categoryId}/posts")
	public ResponseEntity<List<PostDTO>> getPostByCategory(@PathVariable Long categoryId) {
		List<PostDTO> postByCategory = this.postService.getPostByCategory(categoryId);
		return new ResponseEntity<List<PostDTO>>(postByCategory, HttpStatus.OK);

	}

	@GetMapping("/user/{userId}/posts")
	// @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<List<PostDTO>> getPostByUser(@PathVariable Long userId) {
		List<PostDTO> postByUser = this.postService.getPostByUser(userId);
		return new ResponseEntity<List<PostDTO>>(postByUser, HttpStatus.OK);

	}

	@GetMapping("/posts/{postId}")
	// @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<PostDTO> getPostByPostId(@PathVariable Long postId) {
		PostDTO postById = this.postService.getPostByPostId(postId);
		return new ResponseEntity<PostDTO>(postById, HttpStatus.OK);

	}

//by default page number starts from 0;
	@GetMapping("/posts")
	public ResponseEntity<?> getAllPosts(
			@RequestParam(value = "pageNumber", defaultValue = AppConstants.PAGE_NUMBER, required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", defaultValue = AppConstants.PAGE_SIZE, required = false) Integer pageSize,
			@RequestParam(value = "sortBy", defaultValue = AppConstants.SORT_BY, required = false) String sortBy,
			@RequestParam(value = "sortDirection", defaultValue = AppConstants.SORT_DIRECTION, required = false) String sortDirection) {

		PostResponse posts = postService.getAllPosts(pageSize, pageSize, sortBy, sortDirection);
		if (!(posts == null)) {
			return new ResponseEntity<>(posts, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ApiResponse("Posts list is empty", true), HttpStatus.OK);
		}
	}

	@DeleteMapping("posts/{postId}")
	// @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<ApiResponse> deletePost(@PathVariable Long postId) {
		this.postService.deletePost(postId);
		return new ResponseEntity<ApiResponse>(new ApiResponse("post Deleted Successfully", true), HttpStatus.OK);
	}

	@PutMapping("posts/{postId}")
	// @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<PostDTO> updatePost(@Valid @PathVariable Long postId, @RequestBody PostDTO postDTO) {
		PostDTO updatedPost = this.postService.upatePost(postDTO, postId);
		return new ResponseEntity<>(updatedPost, HttpStatus.OK);
	}

	@GetMapping("serach/posts/{keyword}")
//@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<?> getPostsByKeyword(@PathVariable String keyword) {
		List<PostDTO> posts = postService.getPostByKeyword(keyword);

		return new ResponseEntity<>(
				(!posts.isEmpty()) ? posts : new ApiResponse("Post not found with keyword: " + keyword, true),
				HttpStatus.OK);
	}

	// post image upload
	@PostMapping("/post/image/upload/{postId}")
	// @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<PostDTO> uploadPostImage(@RequestParam("image") MultipartFile image,
			@PathVariable Long postId) throws IOException {
		PostDTO postDTO = this.postService.getPostByPostId(postId);
		String fileNmae = this.fileService.uploadImage(path, image);
		postDTO.setImageName(fileNmae);
		//System.out.println("Postimage: " + fileNmae);
		PostDTO updatedPost = this.postService.upatePost(postDTO, postId);
		return new ResponseEntity<PostDTO>(updatedPost, HttpStatus.OK);
	}

//	//method to serve file
	// http://localhost:8080/api/post/image/b00fcbd9-aa6f-4f48-a58f-1f1c854049be.jfif
	@GetMapping(value = "post/image/{imageName}", produces = MediaType.IMAGE_JPEG_VALUE)
	// @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public void downloadImage(@PathVariable("imageName") String imageName, HttpServletResponse response)
			throws IOException {
		InputStream resource = this.fileService.getResource(path, imageName);
		response.setContentType(MediaType.IMAGE_JPEG_VALUE);
		StreamUtils.copy(resource, response.getOutputStream());
	}

}
