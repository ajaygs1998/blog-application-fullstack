package com.blog.payloads;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@NoArgsConstructor
@Getter
@Setter

public class UserDTO {
//+ "\n" +
	private Long id;

	@NotBlank(message = " Name must not be empty!")
	private String username;

	@NotBlank(message = "Email must not be blank")
	@Email(message = "Email must be valid!")
	private String email;

	@Size(min = 8, max = 20, message = "Invalid Password. "
			+ "password must contain min 8 characters & max 20 characters")
	@Pattern(regexp = "^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$", message = "password must includes at least one uppercase letter, one lowercase letter, one number, and one special character.")
	private String password;

	

}
/*
 * Note:
 * 
 * @NotNull : allows blank value
 * 
 * @NotBlank : dont allow white space, allows atleat one character
 * 
 * @NotEmpty : allows white space
 */
