package com.blog.payloads;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter

public class CategoryDTO {

	
	private Long categoryId;
	
	@NotBlank(message="categoryTitle must not be blank")
	private String categoryTitle;
	
	@NotBlank(message= "categoryDescription must not be blank")
	private String categoryDescription;
}
