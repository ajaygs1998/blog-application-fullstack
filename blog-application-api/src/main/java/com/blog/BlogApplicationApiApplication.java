package com.blog;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
@EntityScan(basePackages = {"com.blog.entities"})  // scan JPA entities manually
public class BlogApplicationApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogApplicationApiApplication.class, args);
	}
	
	@Bean
	 ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
