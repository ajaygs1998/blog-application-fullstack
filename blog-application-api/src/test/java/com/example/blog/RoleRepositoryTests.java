package com.example.blog;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.blog.entities.Role;
import com.blog.repositories.RoleRepository;
import com.blog.repositories.UserRepository;

import lombok.extern.slf4j.Slf4j;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
@Slf4j
public class RoleRepositoryTests {
	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserRepository userRepository;

	/*
	 * testCreateRoles() method as JUnit Test in order to insert 3 roles into the
	 * roles table: ADMIN, EDITOR and USER.
	 */
	/*
	 * INSERT INTO roles(name) VALUES('ROLE_USER');
       INSERT INTO roles(name) VALUES('ROLE_MODERATOR');
       INSERT INTO roles(name) VALUES('ROLE_ADMIN');

	 */
	@Test
	public void testCreateRoles() {
		// delete previous roles
		roleRepository.deleteAll();
		Role admin = new Role( "ADMIN");
		Role user = new Role("USER");
		Role editor = new Role("EDITOR");

		List<Role> savedRoles = roleRepository.saveAll(List.of(admin, user, editor));
		log.info("Saved roles: " + savedRoles);
		
        //count() ->Returns the number of entities available.
		long count = roleRepository.count();
		assertEquals(3, count);
	}
	

}