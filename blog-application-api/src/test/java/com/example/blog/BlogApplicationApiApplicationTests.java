package com.example.blog;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.blog.repositories.UserRepository;

@SpringBootTest
class BlogApplicationApiApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	private UserRepository userRepository;

	@Test
	public void TestUserRepository() {
		// classs for the user repo is dynamically created at runtime by jdk. (proxy
		// class)
		String className = this.userRepository.getClass().getName();
		String packageName = this.userRepository.getClass().getPackageName();
		System.out.println("UserRepositories object is created by the class  " + className + ", "
				+ "and it belongs to package " + packageName + " package");

	}

}
